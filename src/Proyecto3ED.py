import cv2
import numpy as np
import random

matriz = []


class Nodo:
  def __init__(self, valor = None, color ="", forma =""):
    global matriz 
    self.valor = valor 
    self.color =color
    self.forma = forma

class grafo:
  def agregar(self, color, forma):
    global matriz 
    valor = len(matriz) + 1
    temp = Nodo(valor, color, forma)
    if(matriz == []):
      matriz = [[temp,0]]
    else:
      matriz = matriz + [[temp]]
      n = len(matriz) - 1
      a = len(matriz) - 1
      x = 0
      for x in range(n):
       tempNumero = random.randint(0,100)
       if (matriz[x][0].forma == matriz[a][0].forma or matriz[x][0].color == matriz[a][0].color):
          matriz[x] = matriz[x] + [tempNumero]
          matriz[a] = matriz[a] + [tempNumero]
       else:
          matriz[x] = matriz[x] + ["-"]
          matriz[a] = matriz[a] + ["-"]
      matriz[a] = matriz[a] + [0]
    del temp

  def imprimir_contenido(self):
    global matriz 
    if matriz == []:
      print("No hay elementos")
    n = len(matriz)
    for x in range(n):
      m = len(matriz[x])
      for y in range(m):
        if (type(matriz[x][y]) != int and type(matriz[x][y]) != str):
          print("El nodo",matriz[x][y].valor,"su forma es un",matriz[x][y].forma,"y el color es",matriz[x][y].color,"y sus coneciones")
        else:
          if(matriz[x][y] != 0 and matriz[x][y] != "-"):
            print(matriz[x][y],"con el nodo", matriz[y-1][0].valor)

  def imprimir_nodo(self, valor):
    global matriz 
    if matriz == []:
      print("No hay elementos")
    n = len(matriz)
    for x in range(n):
      m = len(matriz[x])
      if(matriz[x][0].valor == valor):
        for y in range(m):
          if (type(matriz[x][y]) != int and type(matriz[x][y]) != str):
            print("El nodo",matriz[x][y].valor,"su forma es un",matriz[x][y].forma,"y el color es",matriz[x][y].color, "y sus conexiones")
          else:
            if(matriz[x][y] != 0 and matriz[x][y] != "-"):
              print("\t",matriz[x][y],"con el nodo", matriz[y-1][0].valor)
miGrafo = grafo()

font = cv2.FONT_HERSHEY_COMPLEX

img = cv2.imread("Grafo.jpg")
imgGrey = cv2.imread("Grafo.jpg", cv2.IMREAD_GRAYSCALE)
_, thrash = cv2.threshold(imgGrey, 240, 255, cv2.THRESH_BINARY)
contours, _ = cv2.findContours(thrash, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)


for cnt in contours:
    approx = cv2.approxPolyDP(cnt, 0.01* cv2.arcLength(cnt, True), True)
    cv2.drawContours(img, [approx], 0, (0, 0, 0), 5)
    x = approx.ravel()[0]
    y = approx.ravel()[1] - 5
    if len(approx) == 3:
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"triangulo")
        print("Triangulo")
        cv2.putText(img, "Triangle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
    elif len(approx) == 4:
        x1 ,y1, w, h = cv2.boundingRect(approx)
        aspectRatio = float(w)/h
        if aspectRatio >= 0.95 and aspectRatio <= 1.05:
          miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"cuadrado")
          print("Cuadrado")
          cv2.putText(img, "square", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
        else:
          miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"rectangulo")
          print("Rectangulo")
          cv2.putText(img, "rectangle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
    elif len(approx) == 5:
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"pentagono")
        print("Pentagono")
        cv2.putText(img, "Pentagon", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))
    else:
        miGrafo.agregar(random.choice(["azul", "verde", "amarillo", "rojo"]),"circulo")
        print("Circulo")
        cv2.putText(img, "Circle", (x, y), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 0))


#miGrafo.imprimir_contenido()

miGrafo.imprimir_nodo(1)

"""
cv2.imshow("Grafo", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
"""
